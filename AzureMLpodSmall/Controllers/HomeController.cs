﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AzureMLpodSmall.Models;
using Newtonsoft.Json;
using AzureMLpodSmall.AzureML;

namespace AzureMLpodSmall.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AzureML()
        {
            ViewBag.Message = "Your Azure ML application.";
            ViewBag.Order = "Not predicted";
            
            return View(new OrderViewModel());
        }

        [HttpPost]
        public ActionResult AzureML(OrderViewModel model)
        {
            ViewBag.Message = "Your Azure ML application.";
            var order = new OrderModel();
            
            order.ProductName = model.ProductName;
            order.Buyer = model.Buyer;
            order.Month = model.Month;        

            string predictOrder = AzureMachineLearning.PredictOrder(order);

            ViewBag.Order = predictOrder;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult Submit(AzureMLpodSmall.Models.OrderModels myOrder)
        //{
        //    var my = myOrder;

        //    return View();
        //}
    }
}