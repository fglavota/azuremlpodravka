﻿// This code requires the Nuget package Microsoft.AspNet.WebApi.Client to be installed.
// Instructions for doing this in Visual Studio:
// Tools -> Nuget Package Manager -> Package Manager Console
// Install-Package Microsoft.AspNet.WebApi.Client

using AzureMLpodSmall.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AzureMLpodSmall.AzureML
{

    public class StringTable
    {
        public string[] ColumnNames { get; set; }
        public string[,] Values { get; set; }
    }

    public class AzureMachineLearning
    {
        public static string PredictOrder(OrderModel order)
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {

                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "InputOrder",
                            new StringTable()
                            {
                                ColumnNames = new string[] {"ProductName", "Buyer", "Month"},
                                Values = new string[,] {  { order.ProductName, order.Buyer.ToString(), order.Month } }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };
                const string apiKey = "d6j8nDRjgNX8LGrMavkRE9QyUEsgMvlczfXOUzXPzmdL8qTkBO+K4HzuSwcuIoVw139eIvVToxdZ4GxLiHokIQ=="; // API key for the web service
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress = new Uri("https://europewest.services.azureml.net/workspaces/25c1a8a2bf3a4c34998c387e65dab3ec/services/27e95bc42963428682ca296df0f0133e/execute?api-version=2.0&details=true");

                // Call Web Service
                HttpResponseMessage response = client.PostAsJsonAsync("", scoreRequest).Result;

                //Work with the response
                if (response.IsSuccessStatusCode)
                {
                    string jsonDocument = response.Content.ReadAsStringAsync().Result;

                    var responseBody = JsonConvert.DeserializeObject<RRSResponseObject>(jsonDocument);

                    return responseBody.Results.predictedQuantity.value.Values[0][0];
                }
                else
                {
                    return "Error";
                }
            }
        }

        #region Helper
        private class RRSResponseObject
        {
            public Results Results { get; set; }
        }

        private class Results
        {
            public PredictedQuantity predictedQuantity { get; set; }
        }

        private class PredictedQuantity
        {
            public string type { get; set; }
            public Value value { get; set; }
        }

        private class Value
        {
            public string[] ColumnNames { get; set; }
            public string[] ColumnTypes { get; set; }
            public string[][] Values { get; set; }
        }
        #endregion
    }

}

