﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AzureMLpodSmall.Startup))]
namespace AzureMLpodSmall
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
